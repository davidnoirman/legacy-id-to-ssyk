﻿


##########################
###                    ###
###                    ###
### Not used as of now ###
###                    ###
###                    ###
##########################


class SsykClassifier:
    def __init__(self, use_examined):
        self.use_examined = use_examined

    def determine_meta(self, ssyk_code, ssyk_label, source, alt=None, comment=None):
        return {
            "ssyk_code": ssyk_code,
            "ssyk_label": ssyk_label,
            "source": source,
            "alt": alt,
            "comment": comment,
        }

    def handle_examined_code(self, observed_occupation_item, examined_codes):
        legacy_id = observed_occupation_item["legacy_id"]
        examined_code = examined_codes[legacy_id]
        determined_code = examined_code["determined"]
        comment = examined_code["comment"]
        ssyk_label = examined_code["determined_ssyk_label"]
        if "," in determined_code:
            determined_codes = determined_code.split(",")
            return self.determine_meta(
                ssyk_code=None,
                ssyk_label=None,
                source="manually examined code",
                alt=determined_codes,
                comment=comment,
            )
        return self.determine_meta(
            ssyk_code=determined_code,
            ssyk_label=ssyk_label,
            source="manually examined code",
            alt=None,
            comment=comment,
        )

    def determine_ssyk_96(
        self,
        observed_occupation_item,
        occupation_label,
        correspondence_table,
        occupation_name_to_ssyk_96,
        examined_codes=None,
    ):
        #TODO: Fix examined codes
        ssyk_2012_codes = [k for k, _ in correspondence_table.items()]
        ssyk_codes = observed_occupation_item["ssyk_codes"]
        ssyk_labels = observed_occupation_item["ssyk_labels"]
        ssyk_codes_and_labels = list(zip(ssyk_codes, ssyk_labels))
        ssyk_codes_not_in_2012 = [
            x for x in ssyk_codes_and_labels if x[0] not in ssyk_2012_codes
        ]
        ssyk_96_suggestions = [correspondence_table.get(x) for x in ssyk_codes]

        if code := occupation_name_to_ssyk_96.get(occupation_label.lower()):
            return self.determine_meta(
                ssyk_code=code,
                ssyk_label=None,
                source="from scb:s occupation name mapping",
            )

        if ssyk_codes_not_in_2012:
            last_ssyk = ssyk_codes_not_in_2012[-1]
            return self.determine_meta(
                ssyk_code=last_ssyk[0],
                ssyk_label=last_ssyk[1],
                source="ssyk code not in 2012",
            )
        if ssyk_96_suggestions:
            suggestions = [s for s in ssyk_96_suggestions if s and len(s) == 1]
            suggestion = [s for s in suggestions if s[0] not in ssyk_2012_codes]
            if suggestion:
                return self.determine_meta(
                    ssyk_code=suggestion[-1],
                    ssyk_label=None,
                    source="ssyk mapping exists from 2012 to 96",
                )

        return self.determine_meta(None, None, None)

    def determine_ssyk_2012(
        self,
        observed_occupation_item,
        occupation_label,
        job_titles,
        correspondence_table,
        examined_codes=None,
    ):
        """
        Determines SSYK code for SSYK 2012 (if one can be found).
        """
        ssyk_96_codes = [k for k, _ in correspondence_table.items()]
        last_ssyk_code = observed_occupation_item["ssyk_codes"][-1]
        last_ssyk_label = observed_occupation_item["ssyk_labels"][-1]
        taxonomy_versions = observed_occupation_item["taxonomy_versions"]
        ssyk_2012_suggestions = correspondence_table.get(last_ssyk_code)

        if "new-taxonomy" in taxonomy_versions:
            return self.determine_meta(
                ssyk_code=last_ssyk_code,
                ssyk_label=last_ssyk_label,
                source="in new taxonomy",
            )
        if last_ssyk_code not in ssyk_96_codes:
            return self.determine_meta(
                ssyk_code=last_ssyk_code,
                ssyk_label=last_ssyk_label,
                source="ssyk code not in 96",
            )
        if ssyk_2012_suggestions and len(ssyk_2012_suggestions) == 1:
            return self.determine_meta(
                ssyk_code=ssyk_2012_suggestions[0],
                ssyk_label=last_ssyk_label,
                source="ssyk mapping exists from 96 to 2012",
            )
        if job_titles.get(occupation_label.lower()):
            return self.determine_meta(
                ssyk_code=job_titles[occupation_label][0],
                ssyk_label=job_titles[occupation_label][1],
                source="from job title",
            )
        if self.use_examined:
            return self.handle_examined_code(observed_occupation_item, examined_codes)
        return self.determine_meta(None, None, None)
