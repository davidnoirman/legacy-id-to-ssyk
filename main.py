﻿import sys
from create_mappings import export_mappings

if __name__ == "__main__":
    path = sys.argv[1]
    export_mappings(path)
