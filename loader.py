﻿from collections import defaultdict
import utils as u
import re


def load_file(file, cfg):
    """
    Loads file with appropriate function from path specified in cfg.
    """
    files = cfg["init_files"]

    path = files[file]["path"]
    ssyk_version = cfg["cfg"]["type"]
    d = u.LazyDict(
        {
            "legacy_db_data": lambda: u.load_json(path),
            "ssyk_96_to_ssyk_2012": lambda: ssyk_correspondence_table(
                path, ssyk_version
            ),
            "taxonomy_data": lambda: u.load_json(path)["data"]["concepts"],
            "job_titles": lambda: job_title_to_ssyk_code(path),
            "examined_codes_2012": lambda: u.load_csv(path),
            "examined_codes_96": lambda: u.load_csv(path),
            "ssyk_2012_code_to_ssyk_label": lambda: dict(u.load_csv(path)[1:]),
            "occupation_name_to_ssyk_96": lambda: occupation_name_to_ssyk_96(path),
            "ssyk_96_code_to_label": lambda: dict(u.load_csv(path)[1:]),
        }
    )
    return d[file]


def occupation_name_to_ssyk_96(path):
    """
    Returns dict mapping occupation-name concepts to ssyk 96 codes.
    """
    data = u.load_csv(path)
    result = {}
    for row in data[1:]:
        if row[1]:
            label = row[0].lower()
            result[label] = row[1]
    return result


def ssyk_correspondence_table(path, ssyk_version):
    ssykcorr = u.load_csv(path)[1:]
    result = {}
    for row in ssykcorr:
        if any(row):
            ssyk_2012 = re.findall("\d{4}", row[1])
            ssyk_96 = row[0]
            if ssyk_version == "SSYK2012":
                result[ssyk_96] = ssyk_2012
            if ssyk_version == "SSYK96":
                for ssyk_2012_code in ssyk_2012:
                    if result.get(ssyk_2012_code):
                        result[ssyk_2012_code].append(ssyk_96)
                    else:
                        result[ssyk_2012_code] = [ssyk_96]
    return result


def legacy_id_to_examined(examined_codes, ssyk_code_to_ssyk_label, cfg):
    """
    Creates dict containing legacy-id to determined ssyk code and label for such
    codes that have been manually examined.

    Used to populate the DeterminedOccupationItem class.
    """
    ssyk_code_to_label = load_file(ssyk_code_to_ssyk_label, cfg)
    data = load_file(examined_codes, cfg)
    cols = list(map(lambda s: s.strip("\ufeff"), data[0]))
    cols.append("determined_ssyk_label")
    result = {}
    for row in data[1:]:
        legacy_id = row[1]
        ssyk_code = row[4]
        ssyk_label = ssyk_code_to_label.get(ssyk_code, None)
        row.append(ssyk_label)
        result[legacy_id] = dict(zip(cols, row))
    return result


def job_title_to_ssyk_code(path):
    """
    Job titles are tied to occupation-name concepts,
    which in turn are tied to an SSYK code. Job titles
    are usually more specific than occupation-names concepts,
    and in may in some cases represent and old, deprecated
    occupation.

    Therefore we can attemt to map old occupation labels
    to a job title and from its connection to an occupation-name
    concept determine an SSYK code.

    Note that we only want to use this method if
    a job title can only be tied to one (1) SSYK code.

    """
    result = defaultdict(set)
    data = u.load_json(path)["data"]["concepts"]
    for d in data:
        label = d["preferred_label"].lower()
        related = d["related"]
        for occupation in related:
            ssyk = occupation["broader"][0]
            ssyk_code = ssyk["ssyk_code_2012"]
            ssyk_label = ssyk["preferred_label"]
            result[label].add((ssyk_code, ssyk_label))
    return {k: list(*v) for k, v in result.items() if len(v) == 1}
