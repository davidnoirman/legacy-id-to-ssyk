﻿from legacy_id_to_ssyk import LegacyIdToSsyk
import utils


def get_undetermined(cfg):
    """
    Returns occupation objects that could not be reliably determined through existing data.
    These objects need to be reviewed manually.
    """
    legacy_to_ssyk = LegacyIdToSsyk(
        cfg=cfg,
        ssyk_96_to_ssyk_2012="ssyk_96_to_ssyk_2012",
        legacy_data="legacy_db_data",
        taxonomy_data="taxonomy_data",
        job_titles="job_titles",
        examined_codes="examined_codes",
        ssyk_2012_code_to_ssyk_label="ssyk_2012_code_to_ssyk_label",
        use_examined=False,
        type=cfg["cfg"]["type"],
    )
    determined_occupations = legacy_to_ssyk.determine_occupation_items()
    return list(filter(lambda x: not x.is_determined, determined_occupations))


def export_undetermined(path):
    cfg = utils.load_json("config.json")
    utils.to_excel(path, get_undetermined(cfg))
