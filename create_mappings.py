﻿from legacy_id_to_ssyk import LegacyIdToSsyk
import utils as u

def create_mapping_table(cfg):
    mapping = LegacyIdToSsyk(
        cfg=cfg,
    )
    return mapping.determine_occupation_items()


def get_mapping_table():
    cfg = u.load_json("config.json")
    return create_mapping_table(cfg)


def export_mappings(path):
    table = get_mapping_table()
    to_vars = [vars(x) for x in table]
    if path.endswith(".xlsx"):
        u.to_excel(path, to_vars)
    elif path.endswith(".csv"):
        columns = list(to_vars[0].keys())
        rows = [list(x.values()) for x in to_vars]
        table = [columns] + rows
        u.to_csv(path, table)
    elif path.endswith(".json"):
        u.write_to_json(path, to_vars)
    else:
        print("Use one of the following file formats: .xlsx, .csv, .json")


def merge_mappings(ssyk_96_cfg, ssyk_2012_cfg):
    ssyk_96 = LegacyIdToSsyk(
        cfg=ssyk_96_cfg,
    ).determine_occupation_items()

    ssyk_2012 = LegacyIdToSsyk(cfg=ssyk_2012_cfg).determine_occupation_items()

    dict_2012 = {
        "ssyk_code": "ssyk_code_2012",
        "ssyk_label": "ssyk_label_2012",
        "is_determined": "is_determined_ssyk_2012",
        "source": "source_ssyk_2012",
        "alternatives": "alternatives_ssyk_2012",
        "comment": "comment_ssyk_2012",
    }
    dict_96 = {
        "ssyk_code": "ssyk_code_96",
        "ssyk_label": "ssyk_label_96",
        "is_determined": "is_determined_ssyk_96",
        "source": "source_ssyk_96",
        "alternatives": "alternatives_ssyk_96",
        "comment": "comment_96",
    }
    result = {}
    ssyk_96_mappings = list(map(lambda x: u.rename_keys(vars(x), dict_96), ssyk_96))
    ssyk_2012_mappings = list(
        map(lambda x: u.rename_keys(vars(x), dict_2012), ssyk_2012)
    )

    for row in ssyk_96_mappings + ssyk_2012_mappings:
        result.setdefault(row["legacy_id"], {}).update(row)

    return result


def prepare_merged_mappings():
    ssyk_96_cfg = u.get_config("config.json")
    ssyk_2012_cfg = u.get_config("config.json")
    ssyk_96_cfg["cfg"]["type"] = "SSYK96"
    ssyk_2012_cfg["cfg"]["type"] = "SSYK2012"
    return merge_mappings(ssyk_96_cfg, ssyk_2012_cfg)


def export_merged_mappings(path):
    merged_mappings = prepare_merged_mappings().values()
    if path.endswith(".xlsx"):
        u.to_excel(path, merged_mappings)
    if path.endswith(".csv"):
        result = [
            [
                "legacy_id",
                "ssyk_code_96",
                "ssyk_label_96",
                "ssyk_code_2012",
                "ssyk_label_2012",
                "alternatives_96",
                "alternatives_2012",
            ]
        ]
        for row in merged_mappings:
            legacy_id = row["legacy_id"]
            ssyk_code_96 = row["ssyk_code_96"]
            ssyk_label_96 = row["ssyk_label_96"]
            ssyk_code_2012 = row["ssyk_code_2012"]
            ssyk_label_2012 = row["ssyk_label_2012"]
            alternatives_96 = row["alternatives_ssyk_96"]
            alternatives_2012 = row["alternatives_ssyk_2012"]
            result.append(
                [
                    legacy_id,
                    ssyk_code_96,
                    ssyk_label_96,
                    ssyk_code_2012,
                    ssyk_label_2012,
                    alternatives_96,
                    alternatives_2012,
                ]
            )

        u.to_csv(path, result)

