﻿# Readme

## Getting started

The mapping files can be found in the [output folder](output). If the examined_codes file has been upated, new mapping files need to be created. To create new files, follow these steps:

### Install dependencies

`pip install -r requirements.txt`

### Run script

Use `python3 main.py output/{filename}.{format}` to create a new mapping file (supported formats are .json, .xlsx and .csv)

The resulting file will be based on the configuration. See [config](#config) for more info.

## Purpose

The purpose of this project is to provide mappings between [occupation-name](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/yrkesben%C3%A4mningar.md?ref_type=heads) concept's legacy-id's from both the [Jobtech Taxonomy](https://jobtechdev.se/sv/komponenter/jobtech-taxonomy) and the old, deprecated taxonomy solution, and [SSYK level 4](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/ssyk_niv%C3%A5_4.md?ref_type=heads) groups.

The mappings can be helpful when analysing [job ads](https://data.jobtechdev.se/annonser/historiska/index.html) predating 2016 (2017?), specifically in cases where data is aggregated to the SSYK level 4 groups. In the metadata of the pre 2016 ads, no such groups are specified, only occupation-name concepts together with their legacy-ids (ids from the old, deprecated, taxonomy solution).  

By combining different sources of data and exising connections between concepts in both the old and the new taxonomy solutions, this software tries to ascertain mappings between occupation-name concepts and SSYK level 4 groups. In many cases however, where the data at hand has been insufficient to reliably assign a legacy-id to an SSYK level 4 group, the mappings have been done manually by an expert at Arbetsförmedlingen. For more specifics on the mapping procedure, see the Method chapters below.

The mappings include SSYK2012 as well as SSKY96.

## The problem

Are connections between occupation-name concepts and SSYK level 4 groups not already avaliable in the taxonomy?

Yes and no. All occupation-name concepts, in both the new and old taxonomy solutions, are related to an SSYK level 4 group, which has an SSYK code. The issue comes when we try to trace the code to either SSYK2012 or SSYK96, especially in the old taxonomy solution.

Some of the issues:

- concepts that have only existed during either SSYK96 or SSYK2012 are not inherently mapped to the other SSYK version
- occupation-name concepts in the old taxonomy has information on SSYK codes (often multiple per concept), but it is not always apparent whether or not a code is tied to SSYK2012 or SSYK96 since codes are reused between the versions
- codes can only be translated with SCB's corresponce table when an SSYK group in one SSYK version is mapped towards a single group in the other (which is generally not the case)

Mappings for these cases are attempted in this project.

## Mappings

The created mappings can be found in the [output folder](output).

## Known issues or constraints

- not all occupation-name concepts are mappable (e.g. the old, deprecated concept "Franschisetagare")
- the meaning of some occupation-name concepts are not always apparent, especially older ones, and are thus hard to assign to an SSYK level 4 group
- occupation-name concepts that have been deprecated for a long time may not have an apparent target group in SSYK2012
- likewise, newer occupation-name concepts may not always have an apparent target group in SSYK96
- only occupation-name concepts with a legacy-id are included in the mappings
  - concepts created in the new database does not have legacy-id:s

## Config

In the [config file](config.json) you can specify type and whether or not to include manually examined mappings, where:

- `type` is set to either `SSYK96` or `SSYK2012`
- `use_examined` is set to either `true` or `false`

If `use_examined` is set to `false`, a large proportion of the mappings will be undetermined.

Paths to each resource used is also specified here.

## Resources used

Files that are listed below represents data that have been used to help determine a connection between a legacy-id and an SSYK 2012 code.

See [config file](config.json) for specific paths.

### legacy_db_data

Data from the old taxonomy database. Many of the concepts found here are not present in the new database.

Each row represents an occupation-name concept in a certain version of the taxonomy.

This data spans both SSYK96 and SSYK2012, and points out an SSYK4 code for each occupation-name concept. However it does not state, for any occupation, which SSYK version its corresponding SSYK code belongs to. Mapping these codes are the main consideration of this project.

### taxonomy_data

Data from the new taxonomy database.

If an occupation-name concept is present in this data, the related SSYK4 code belongs to SSYK2012.

Based on following query:

```graphql
query MyQuery {
  concepts(type: "ssyk-level-4") {
    id
    preferred_label
    type
    ssyk_code_2012
    narrower(type: "occupation-name", include_deprecated: true) {
      id
      preferred_label
      type
      deprecated
      deprecated_legacy_id
    }
  }
}
```
Through: https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql

### ssyk_96_to_ssyk_2012

Correspondence table between SSYK96 and SSYK2012. Created by SCB.

### job_titles

Job titles from the new taxonomy, togther with relations to occupation-name concepts and, through them, ssyk-level-4 concepts. Used to map old occupation-name labels to job-titles to derive a SSYK4 code suggestion.

Based on the following query:

```graphql
query MyQuery {
  concepts(type: "job-title", include_deprecated: true) {
    id
    preferred_label
    type
    related(type: "occupation-name", include_deprecated: true) {
      id
      preferred_label
      type
      broader(type: "ssyk-level-4") {
        id
        preferred_label	
        type
        ssyk_code_2012
      }
    }
  }
}
```

Through: https://taxonomy.api.jobtechdev.se/v1/taxonomy/graphiql

### examined_codes_2012

Legacy-ids that have been examined and mapped to an SSYK4 (in SSYK2012) code manually.

Note that in some instances no SSYK4 code has been identified, but instead two codes suggested. These cases occur when an occupation and its SSYK4 group has been split in SSYK2012. See, for example, the old occupation-name concept "Undersköterska". In SSYK96 this concept was tied to the SSYK4 group "5132 - Undersköterskor, sjukvårdsbiträden m.fl.". In SSYK2012, this group was split and two new groups for "Undersköterskor" was created: "5321 - Undersköterskor, hemtjänst, hemsjukvård, äldreboende och habilitering" and "5323 - Undersköterskor, vård- och specialavdelning och mottagning". Since the term "Undersköterska" is more general than the SSYK4 groups, it is not possible to tie the concept to any one of them. Here, a separate field is used instead, called `alternatives`, listing the two new SSYK4 codes.

For one occupation-name concept, "Franschisetagare", neither an SSYK4 code or any alternatives could be determined, since the concept could be related to a multitude of SSYK4 groups.

### examined_codes_96

Legacy-ids that have been examined and mapped to an SSYK4 (in SSYK96) code manually.

### ssyk_2012_code_to_ssyk_label

SSYK4 codes and their corresponding labels

### occupation_name_to_ssyk_96

[SCB:s list](https://www.scb.se/dokumentation/klassifikationer-och-standarder/standard-for-svensk-yrkesklassificering-ssyk/) of occupation-name labels and their corresponding SSYK96 codes.

### ssyk_96_code_to_label

SSYK96 codes and their corresponding labels. List created by SCB.

## Definitions

### SSYK level 4 group

A group from the SSYK structure's most detailed level. In the Taxonomy, these have a number of occupation-name concepts tied to them.

### Occupation-name concepts

An occupation-name concept is a representation of an occupation in the Taxonomy database. These concepts consists of a number of attributes, such as `legacy-id`, `preferred-label`, `definition`, and so on. In the Taxonomy, these concepts are always tied to an SSYK level 4 group.

## Method: SSYK2012

First step is to collect avaliable occupation-name concepts found in both current and legacy databases. Each observed concept is represented as an `ObservedOccupationItem` object. Since a concept can be observed multiple times throughout the databases, each object usually contains a number of SSYK codes, labels, etc.

The following step is dependant on the data that is avaliable to us in order to determine an SSYK code to connect the occupation-name concept to.

### Case 1 - Concept exists in the new Taxonomy

Occupation-name concepts with legacy-id's that exists in the new Taxonomy are always tied to an SSYK4 group from SSYK2012. In these cases, that connection is always used to determine the SSYK4 code. In these cases we want to choose the chronologically last SSYK-code, since some occupation-name concepts might have changed SSYK-codes during SSYK2012's lifespan, due to having been tied to an incorrect one at an earlier stage of it.

The connections are created by an editorial team at Arbetsförmedlingen.

When finalized as a `DeterminedOccupationItem`, `source` is set to "in new taxonomy"

### Case 2 - SSYK code is unique for SSYK2012

If an occupation-name concept does not exist in the new Taxonomy, but was removed at a relatively late point in the old database, we can check if the latest registered SSYK code is unique for SSYK2012. If so, we pair that code with the occupation-name concept's legacy id.

When finalized as a `DeterminedOccupationItem`, `source` is set to "ssyk code not in 96"

### Case 3 - SSYK96 code is mapped to only one SSYK2012 code

In many cases the correspondence table provided by SCB specifies mappings from one SSYK96 group to many SSYK2012 groups. For occupation-name concepts tied to such groups in SSYK96, we cannot reliably determine which of the mapped SSYK2012 groups to tie it to. However, in cases of 1:1 mappings, we can use the latest registered SSYK96 code for an occupation-name concept in the legacy database to find its corresponding code in SSYK2012.

When finalized as a `DeterminedOccupationItem`, `source` is set to "equal in 96 and 2012"

### Case 4 - Occupation-name label exists as a job-title

A [job-title](https://gitlab.com/arbetsformedlingen/taxonomy-content/taxonomy-content-documentation/content/-/blob/main/doc_sv/jobbtitlar.md?ref_type=heads) is a usually more narrow/specific concept connected to an occupation-name concept. These concepts exists in the new taxonomy solution, and are in many cases old occupation-name concepts that have been deprecated.

When finalized as a `DeterminedOccupationItem`, `source` is set to "from job title"

### Case 5 - None of the previous cases are applicable

In cases where no mapping has been determined through any of the previous sources, the occupation-name concept has been mapped manually to an SSYK level 4 group. These cases only concern occupation name concepts that have never existed in the new Taxonomy database. Items gathered from the new taxonomy are always tied to SSYK level 4 groups from the 2012 version.

When mapping such occupation-name concepts to SSYK level 4 groups, we drew information from the SSYK level 4 groups' definitions and what other occupation name concepts are tied to them, as well as skills and other types of concepts avaliable in the Taxonomy.

## Method: SSYK96

The first step is to collect avaliable occupation-name concepts found in both current and legacy databases. Each observed concept is represented as an `ObservedOccupationItem` object. Since a concept can be observed multiple times throughout the databases, each object usually contains a number of SSYK codes, labels, etc.

The following step is dependant on the data that is avaliable to us in order to determine an SSYK code to connect the occupation-name concept to.

### Case 1 - Occupation-name label exists in SCB's mapping file

When a label is matched in the `occupation_name_to_ssyk_96`, the corresponding SSYK level 4 group in the file is chosen.

### Case 2 - SSYK code is unique for SSYK96

If all SSYK level 4 groups tied to an occupation-name concepts have SSYK codes unique for SSYK96, the latest of the observed codes is chosen.

When finalized as a `DeterminedOccupationItem`, `source` is set to "ssyk code not in 2012"

### Case 3 - SSYK2012 code is mapped to only one SSYK96 code

This method was not used. Mappings for these cases were handled manually.

### Case 4 - None of the previous cases are applicable

In cases where no mapping has been determined through any of the previous sources, the occupation-name concept has been mapped manually to an SSYK level 4 group.

When mapping such occupation-name concepts to SSYK level 4 groups, we drew information from the SSYK level 4 groups' definitions, provided by SCB, and what other occupation name concepts were tied to them back when the structure was in use. Online sources have also been used in order to determine the meaning of some older occupation-name concepts.

## Output

The finished mapping files are found in the [output folder](/output/). The formats avaliable are .json, .csv and .xlsx.

Each file consists of the following columns/keys: 

- `legacy_id`: old taxonomy-id representing the occupation-name concept
- `occupation_label`: the occupation-name concept's [preferred label](http://www.w3.org/2004/02/skos/core#prefLabel)
- `ssyk_code`: determined SSYK level 4 group's SSYK-code
- `ssyk_label`: determined SSYK level 4 group's [preferred label](http://www.w3.org/2004/02/skos/core#prefLabel)
- `is_determined`: `bool`, usually `true`. If `false`, no SSYK-level-4 group could be reliabliy determined
- `source`: represents the source of the determined SSYK level 4 group, e.g. `manually examined code`
- `alternatives`: usually `null`, only used in cases where an occupation-name concept could not be tied to one, but rather two, SSYK level 4 groups, e.g. "Undersköterska" in the SSYK2012 mapping file. When this field is populated with a list of values, both `ssyk_code` and `ssyk_labels` should be `null`
- `comment`: sparsely used, mostly in cases where an explanation for the mapping seemed necessary


## TODO
- use graphql instead of local files for taxonomy data