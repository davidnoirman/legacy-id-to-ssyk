﻿import json
import csv
import pandas as pd

def load_json(path):
    with open(path, 'r', encoding="utf-8") as reader:
        return json.load(reader)
    
def write_to_json(path, data):
    json_object = json.dumps(data, indent=4)
    with open(path, "w") as outfile:
        outfile.write(json_object)


def get_config(path):
    return load_json(path)

def get_paths(config):
    return config["init_files"]

def load_csv(path):
    result = []
    with open(path, encoding="utf-8") as file:
        reader = csv.reader(file)
        for row in reader:
            result.append(row)
    return result

def to_excel(path_out, data):
    ## remove later
    df = pd.DataFrame(data)
    df.to_excel(path_out, index=False)
    
    
def map_list_vals(observed_occupation_item, k1, k2):
    """
    Zip two lists into a dict
    """
    return dict(zip(observed_occupation_item[k1],
                    observed_occupation_item[k2]))


class LazyDict(dict):
    def __init__(self, lazies):
        self.lazies = lazies
    def __missing__(self, key):
        value = self[key] = self.lazies[key]()
        return value

def rename_keys(d, ks):
    result = {}
    for k, v in d.items():
        new_key = ks.get(k, k)
        result[new_key] = v
    return result

def to_csv(path_out, data):
    with open(path_out, 'w', encoding="utf-8") as file:
        writer = csv.writer(file)
        for x in data:
            writer.writerow(x)