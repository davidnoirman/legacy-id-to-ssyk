﻿"""
Legacy ID to SSYK
"""
from dataclasses import dataclass
import loader as l


@dataclass
class ObservedOccupationItem:
    """
    Occupation name item
    """

    legacy_id: str
    labels: list
    ssyk_codes: list
    ssyk_labels: list
    ssyk_versions: list
    taxonomy_versions: list


@dataclass
class DeterminedOccupationItem:
    """
    Attrs:
        is_determined: if False, no single SSYK code could be reliably determined.

        source: States the source of the determination.

        alternatives: Two suggestions for SSYK2012 codes. Only used in cases when no single
        SSYK2012 code could be reliably determined.

        comment: sparsely used, only in case of manually determined codes.
    """

    legacy_id: str
    occupation_label: str
    ssyk_code: str
    ssyk_label: str
    is_determined: bool
    source: str
    alternatives: list
    comment: str


class LegacyIdToSsyk:
    # TODO: Break out determine_ssyk methods to own class
    """
    If use_examined_codes is True, manually determined SSYK codes
    will be included in the result. Set to False to get occupation
    objects for which an SSYK code could not be reliably determined
    through existing data and have to be reviewed manually.
    """

    def __init__(self, **attrs):
        self.cfg = attrs["cfg"]
        self.use_examined = self.cfg["cfg"]["use_examined"]
        self.ssyk_version = self.cfg["cfg"]["type"]
        
    def conform_taxonomy_item(self, taxonomy_item):
        """
        An item represents an SSYK group with its related occupations.
        The function fits the item in a standardized data structure.
        """
        result = []
        ssyk_code = taxonomy_item["ssyk_code_2012"]
        ssyk_label = taxonomy_item["preferred_label"]
        occupations = taxonomy_item["narrower"]
        taxonomy_version = "new-taxonomy"
        ssyk_version = "2012"
        for occupation in occupations:
            legacy_id = occupation["deprecated_legacy_id"]
            occupation_label = occupation["preferred_label"]
            if legacy_id:
                result.append(
                    ObservedOccupationItem(
                        legacy_id=legacy_id,
                        labels=[occupation_label],
                        ssyk_codes=[ssyk_code],
                        ssyk_labels=[ssyk_label],
                        ssyk_versions=[ssyk_version],
                        taxonomy_versions=[taxonomy_version],
                    )
                )
        return result

    def conform_taxonomy_data(self, taxonomy_data):
        """
        Conform all values from the new taxonomy.
        """
        result = []
        for item in taxonomy_data:
            conformed = self.conform_taxonomy_item(item)
            for occupation in conformed:
                result.append(occupation)
        return result

    def conform_legacy_item(self, legacy_item):
        """
        An item represents an occupation item from a point in time (from legacy db).
        The function fits the item in a standardized data structure.
        """
        ssyk_code = legacy_item["ssyk"]
        ssyk_label = legacy_item["ssyk_term"]
        legacy_id = str(legacy_item["legacy_occupation_name_id"])
        occupation_label = legacy_item["legacy_occupation_name_term"]
        taxonomy_version = str(legacy_item["legacy_version"])
        ssyk_version = "unknown"
        return ObservedOccupationItem(
            legacy_id=legacy_id,
            labels=[occupation_label],
            ssyk_codes=[ssyk_code],
            ssyk_labels=[ssyk_label],
            ssyk_versions=[ssyk_version],
            taxonomy_versions=[taxonomy_version],
        )

    def conform_legacy_data(self, legacy_data):
        """
        Conform all values from the legacy database.
        """
        return [self.conform_legacy_item(item) for item in legacy_data]

    def group_by_legacy_id(self, legacy_data, taxonomy_data):
        """
        Group all entries by legacy id
        """
        legacy_conformed = self.conform_legacy_data(legacy_data)
        taxonomy_conformed = self.conform_taxonomy_data(taxonomy_data)
        data = legacy_conformed + taxonomy_conformed
        grouped = {}
        for item in data:
            entry = vars(item)
            legacy_id = entry["legacy_id"]
            if not legacy_id in grouped:
                grouped[legacy_id] = entry
            else:
                for k, v in grouped[legacy_id].items():
                    if isinstance(v, list):
                        grouped[legacy_id][k] = v + entry[k]
                    else:
                        grouped[legacy_id][k] = entry[k]
        return grouped

    def determine_meta(self, ssyk_code, ssyk_label, source, alt=None, comment=None):
        return {
            "ssyk_code": ssyk_code,
            "ssyk_label": ssyk_label,
            "source": source,
            "alt": alt,
            "comment": comment,
        }

    def determine_occupation_label(self, observed_occupation_item):
        """
        Pass a grouped observed item.
        Picks latest observed label.
        """
        return observed_occupation_item["labels"][-1]

    def handle_examined_code(self, observed_occupation_item, examined_codes):
        legacy_id = observed_occupation_item["legacy_id"]
        examined_code = examined_codes[legacy_id]
        determined_code = examined_code["determined"]
        comment = examined_code["comment"]
        ssyk_label = examined_code["determined_ssyk_label"]
        if "," in determined_code:
            determined_codes = determined_code.split(",")
            return self.determine_meta(
                ssyk_code=None,
                ssyk_label=None,
                source="manually examined code",
                alt=determined_codes,
                comment=comment,
            )
        return self.determine_meta(
            ssyk_code=determined_code,
            ssyk_label=ssyk_label,
            source="manually examined code",
            alt=None,
            comment=comment,
        )

    def determine_ssyk_96(
        self,
        observed_occupation_item,
        occupation_label,
        correspondence_table,
        occupation_name_to_ssyk_96,
        ssyk_96_code_to_label,
        examined_codes=None,
    ):
        ssyk_2012_codes = [k for k, _ in correspondence_table.items()]
        ssyk_codes = observed_occupation_item["ssyk_codes"]
        ssyk_labels = observed_occupation_item["ssyk_labels"]
        legacy_id = observed_occupation_item["legacy_id"]
        ssyk_codes_and_labels = list(zip(ssyk_codes, ssyk_labels))
        ssyk_codes_not_in_2012 = [
            x for x in ssyk_codes_and_labels if x[0] not in ssyk_2012_codes
        ]
        ssyk_96_suggestions = [correspondence_table.get(x) for x in ssyk_codes]
        
        if self.use_examined and examined_codes.get(legacy_id):
            return self.handle_examined_code(observed_occupation_item, examined_codes)

        if code := occupation_name_to_ssyk_96.get(occupation_label.lower()):
            return self.determine_meta(
                ssyk_code=code,
                ssyk_label=ssyk_96_code_to_label.get(code),
                source="from scb:s occupation name mapping",
            )

        if ssyk_codes_not_in_2012:
            last_ssyk = ssyk_codes_not_in_2012[-1]
            return self.determine_meta(
                ssyk_code=last_ssyk[0],
                ssyk_label=last_ssyk[1],
                source="ssyk code not in 2012",
            )
        ## Below not used as of now
        """if ssyk_96_suggestions:
            suggestions = [s for s in ssyk_96_suggestions if s and len(s) == 1]
            if suggestions:
                suggestion = suggestions[-1][0]
                return self.determine_meta(
                    ssyk_code=suggestion,
                    ssyk_label=ssyk_96_code_to_label.get(suggestion),
                    source="ssyk mapping exists from 2012 to 96",
                )
"""
        return self.determine_meta(None, None, None)

    def determine_ssyk_2012(
        self, observed_occupation_item, job_titles, correspondence_table, examined_codes
    ):
        ssyk_96_codes = [k for k, _ in correspondence_table.items()]
        last_ssyk_code = observed_occupation_item["ssyk_codes"][-1]
        last_ssyk_label = observed_occupation_item["ssyk_labels"][-1]
        taxonomy_versions = observed_occupation_item["taxonomy_versions"]
        legacy_id = observed_occupation_item["legacy_id"]
        ssyk_2012_suggestions = correspondence_table.get(last_ssyk_code)
        occupation_label = self.determine_occupation_label(
            observed_occupation_item
        ).lower()
        if self.use_examined and examined_codes.get(legacy_id):
            return self.handle_examined_code(observed_occupation_item, examined_codes)
        if "new-taxonomy" in taxonomy_versions:
            return self.determine_meta(
                ssyk_code=last_ssyk_code,
                ssyk_label=last_ssyk_label,
                source="in new taxonomy",
            )
        if last_ssyk_code not in ssyk_96_codes:
            return self.determine_meta(
                ssyk_code=last_ssyk_code,
                ssyk_label=last_ssyk_label,
                source="ssyk code not in 96",
            )
        if ssyk_2012_suggestions and len(ssyk_2012_suggestions) == 1:
            return self.determine_meta(
                ssyk_code=ssyk_2012_suggestions[0],
                ssyk_label=last_ssyk_label,
                source="ssyk mapping exists from 96 to 2012",
            )
        if job_titles.get(occupation_label):
            return self.determine_meta(
                ssyk_code=job_titles[occupation_label][0],
                ssyk_label=job_titles[occupation_label][1],
                source="from job title",
            )
       
        return self.determine_meta(None, None, None)

    def determine_occupation_item(
        self,
        observed_occupation_item,
        correspondence_table,
        examined_codes,
        ssyk_96_code_to_label=None,
        job_titles=None,
        occupation_name_to_ssyk_96=None,
    ):
        label = self.determine_occupation_label(observed_occupation_item)
        if self.ssyk_version == "SSYK2012":
            observed = self.determine_ssyk_2012(
                observed_occupation_item=observed_occupation_item,
                job_titles=job_titles,
                correspondence_table=correspondence_table,
                examined_codes=examined_codes,
            )
        elif self.ssyk_version == "SSYK96":
            observed = self.determine_ssyk_96(
                observed_occupation_item=observed_occupation_item,
                occupation_label=label,
                correspondence_table=correspondence_table,
                examined_codes=examined_codes,
                occupation_name_to_ssyk_96=occupation_name_to_ssyk_96,
                ssyk_96_code_to_label=ssyk_96_code_to_label,
            )
        ssyk_code = observed["ssyk_code"]
        ssyk_label = observed["ssyk_label"]
        source = observed["source"]
        legacy_id = observed_occupation_item["legacy_id"]
        is_determined = bool(ssyk_code)
        alt = observed["alt"]
        comment = observed["comment"]

        return DeterminedOccupationItem(
            legacy_id=legacy_id,
            occupation_label=label,
            ssyk_code=ssyk_code,
            ssyk_label=ssyk_label,
            is_determined=is_determined,
            source=source,
            alternatives=alt,
            comment=comment,
        )

    def determine_occupation_items(self):
        legacy = l.load_file("legacy_db_data", self.cfg)
        taxonomy = l.load_file("taxonomy_data", self.cfg)
        all_grouped = self.group_by_legacy_id(legacy, taxonomy)
        ssyk_correspondence_table = l.load_file("ssyk_96_to_ssyk_2012", self.cfg)

        if self.ssyk_version == "SSYK2012":
            examined_codes = (
                l.legacy_id_to_examined(
                    "examined_codes_2012", "ssyk_2012_code_to_ssyk_label", self.cfg
                )
                if self.use_examined
                else None
            )
            job_titles = l.load_file("job_titles", self.cfg)
            return [
                self.determine_occupation_item(
                    observed_occupation_item=v,
                    job_titles=job_titles,
                    correspondence_table=ssyk_correspondence_table,
                    examined_codes=examined_codes,
                )
                for _, v in all_grouped.items()
            ]
        if self.ssyk_version == "SSYK96":
            ssyk_96_code_to_label = l.load_file("ssyk_96_code_to_label", self.cfg)
            occupation_name_to_ssyk_96 = l.load_file(
                "occupation_name_to_ssyk_96", self.cfg
            )
            examined_codes = (
                l.legacy_id_to_examined(
                    "examined_codes_96", "ssyk_96_code_to_label", self.cfg
                )
                if self.use_examined
                else None
            )
            return [
                self.determine_occupation_item(
                    observed_occupation_item=v,
                    occupation_name_to_ssyk_96=occupation_name_to_ssyk_96,
                    correspondence_table=ssyk_correspondence_table,
                    examined_codes=examined_codes,
                    ssyk_96_code_to_label=ssyk_96_code_to_label,
                )
                for _, v in all_grouped.items()
            ]
